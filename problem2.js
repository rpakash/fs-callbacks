const fs = require("fs");
const path = require("path");

function problem2() {
  fs.readFile(path.join(__dirname, "lipsum.txt"), (err, data) => {
    if (err) {
      throw new Error(err);
    } else {
      fs.writeFile(path.join(__dirname, "upper.txt"), data.toString().toLocaleUpperCase(), (err) => {
        if (err) {
          throw new Error(err);
        } else {
          fs.writeFile(path.join(__dirname, "filenames.txt"), "upper.txt", (err) => {
            if (err) {
              throw new Error(err);
            } else {
              fs.readFile(path.join(__dirname, "upper.txt"), (err, data) => {
                if (err) {
                  throw new Error(err);
                } else {
                  const lower = data.toString().toLocaleLowerCase().split(".");

                  fs.writeFile(path.join(__dirname, "lower.txt"), lower.join("\n"), (err) => {
                    if (err) {
                      throw new Error(err);
                    } else {
                      fs.appendFile(path.join(__dirname, "filenames.txt"), "\nlower.txt", (err) => {
                        if (err) {
                          throw new Error(err);
                        } else {
                          fs.readFile(path.join(__dirname, "lower.txt"), (err, data) => {
                            if (err) {
                              throw new Error(err);
                            } else {
                              let bothData = data.toString().split(" ");

                              fs.readFile(path.join(__dirname, "upper.txt"), (err, data) => {
                                if (err) {
                                  throw new Error(err);
                                } else {
                                  //Add the data from both files to array for sorting
                                  bothData = bothData.concat(data.toString().split(" "));

                                  bothData.sort((wordA, wordB) => {
                                    return wordA.localeCompare(wordB);
                                  });

                                  fs.writeFile(path.join(__dirname, "sort.txt"), bothData.join("\n"), (err) => {
                                    if (err) {
                                      throw new Error(err);
                                    } else {
                                      fs.appendFile(path.join(__dirname, "filenames.txt"), "\nsort.txt", (err) => {
                                        if (err) {
                                          throw new Error(err);
                                        } else {
                                          fs.readFile(path.join(__dirname, "filenames.txt"), (err, data) => {
                                            if (err) {
                                              throw new Error(err);
                                            } else {
                                              const fileNames = data.toString().split("\n");

                                              for (let index = 0; index < fileNames.length; index++) {
                                                fs.unlink(path.join(__dirname, fileNames[index]), (err) => {
                                                  if (err) {
                                                    throw new Error(err);
                                                  }
                                                });
                                              }
                                            }
                                          });
                                        }
                                      });
                                    }
                                  });
                                }
                              });
                            }
                          });
                        }
                      });
                    }
                  });
                }
              });
            }
          });
        }
      });
    }
  });
}

module.exports = problem2;
