const fs = require("fs");
const path = require("path");

function problem1(callback) {
  const dirName = "randomDir";
  const dirPath = path.join(__dirname, dirName);

  fs.mkdir(dirPath, (err) => {
    if (err) {
      console.Error("Error creating directory");

      callback(err);
    } else {
      console.log("Created directory");

      callback(null, dirPath, deleteFiles);
    }
  });
}

function writeFiles(err, directory, callback) {
  if (err) {
    throw new Error(err);
  } else {
    const fileNames = Array(10)
      .fill(0)
      .map((_, index) => {
        return `${index}.json`;
      });
    let countWriting = 0;

    for (let index = 0; index < fileNames.length; index++) {
      const data = { randomNumber: Math.random() * 10000 };
      const fileName = path.join(directory, fileNames[index]);

      fs.writeFile(fileName, JSON.stringify(data), (err) => {
        countWriting += 1;

        if (err) {
          callback(err);
        } else if (countWriting === fileNames.length) {
          // Call deleteFiles function only when all the files are written
          console.log("All Files created");

          callback(null, directory, fileNames);
        }
      });
    }
  }
}

function deleteFiles(err, directory, fileNames) {
  if (err) {
    throw new Error(err);
  } else {
    let deletedFiles = 0;
    const totalFiles = fileNames.length;

    for (let index = 0; index < fileNames.length; index++) {
      fs.unlink(path.join(directory, fileNames[index]), (err) => {
        deletedFiles += 1;

        if (err) {
          console.log("Failed to delete file");

          throw new Error(err);
        } else if (deletedFiles === totalFiles) {
          // Remove directory only when all the files are deleted.
          console.log("All files deleted");

          fs.rmdir(directory, (err) => {
            if (err) {
              console.log("Failed to delete directory");
              throw new Error(err);
            } else {
              console.log("Directory deleted");
            }
          });
        }
      });
    }
  }
}

module.exports = { problem1, writeFiles, deleteFiles };
